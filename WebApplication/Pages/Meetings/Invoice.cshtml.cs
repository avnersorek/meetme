﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Meetings
{
    public class InvoiceModel : PageModel
    {
        private readonly IMeetingService _meetingService;
        private readonly IUserContextService _userContextService;

        public Meeting Meeting { get; set; }        

        public InvoiceModel(IMeetingService meetingService, IUserContextService userContextService)
        {
            _meetingService = meetingService;
            _userContextService = userContextService;
        }

        public IActionResult OnGet(int meetingId)
        {
            Meeting = _meetingService.GetMeetingById(meetingId);
            int loggedInUserId = _userContextService.GetLoggedInUserId();

            if (loggedInUserId != Meeting.CustomerId && 
                loggedInUserId != Meeting.ProvidedService.ServiceProviderId)
                return Unauthorized();

            return Page();
        }
    }
}