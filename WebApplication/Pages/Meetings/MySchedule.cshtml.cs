﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Meetings
{
    public class MyScheduleModel : PageModel
    {
        private readonly ISchedulingService _schedulingService;
        private readonly IUserContextService _userContextService;

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public List<DateTime> Days { get; set; }
        public List<(string, ScheduleSlot[])> SlotsByHours { get; set; }
        public bool ShowCustomerNames { get; set; }

        private const int SCHEDULE_DAYS_TO_SHOW = 7;

        public MyScheduleModel(ISchedulingService schedulingService, IUserContextService userContextService)
        {
            _schedulingService = schedulingService;
            _userContextService = userContextService;
        }

        public void OnGet()
        {
            StartTime = DateTime.Today; // start of day
            EndTime = StartTime.AddDays(SCHEDULE_DAYS_TO_SHOW).AddMinutes(-1); // end of day 
            Days = _schedulingService.GetDaysInRange(StartTime, EndTime);

            User user = _userContextService.GetLoggedInUser();

            SlotsByHours = _schedulingService.GetScheduleForUser(user.UserId, StartTime, EndTime);
            ShowCustomerNames = user.Role == Core.Models.User.RoleType.ServiceProvider;
        }
    }
}