﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Admin
{
    public class ServiceTypesModel : PageModel
    {
        private readonly IDictionaryService _dictionaryService;
        private readonly IUserContextService _userContextService;

        [BindProperty]
        public string NewServiceTypeName { get; set; }

        public List<ServiceType> ServiceTypes { get; set; }
        
        public ServiceTypesModel(IDictionaryService dictionaryService, IUserContextService userContextService)
        {
            _userContextService = userContextService;
            _dictionaryService = dictionaryService;
            ServiceTypes = _dictionaryService.GetServiceTypes();
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid) return Page();

            User currentUser = _userContextService.GetLoggedInUser();
            if (currentUser.Role != Core.Models.User.RoleType.Admin) return Unauthorized();

            ServiceType newServiceType = new ServiceType()
            {
                Name = NewServiceTypeName
            };

            try
            {
                _dictionaryService.AddServiceType(newServiceType);
                // For page reload
                ModelState.Clear();
                ServiceTypes = _dictionaryService.GetServiceTypes();
            } 
            catch (EntityAlreadyExistsException)
            {
                ModelState.AddModelError("NewServiceTypeName", "Service Type already exists");
            }

            return Page();
        }
    }
}