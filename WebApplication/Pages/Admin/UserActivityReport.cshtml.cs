﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Admin
{
    public class UserActivityReportModel : PageModel
    {
        private readonly IUserActivityService _userActivityService;
        private readonly IUserContextService _userContextService;

        public int Year { get; set; }
        public int Month { get; set; }
        public List<UserActivity> UserActivities { get; set; }

        public SelectList YearOptions { get; set; }
        public SelectList MonthOptions { get; set; }

        public UserActivityReportModel(IUserActivityService userActivityService, IUserContextService userContextService)
        {
            _userActivityService = userActivityService;
            _userContextService = userContextService;
            FillOptions();
        }

        public IActionResult OnGet()
        {
            User currentUser = _userContextService.GetLoggedInUser();
            if (currentUser.Role != Core.Models.User.RoleType.Admin) return Unauthorized();

            Year = (Request.Query.ContainsKey("year")) ? int.Parse(Request.Query["year"]) : DateTime.Now.Year;
            Month = (Request.Query.ContainsKey("month")) ? int.Parse(Request.Query["month"]) : DateTime.Now.Month;

            UserActivities = _userActivityService.GetActivityForMonth(Year, Month);
            UserActivities.Sort((a,b) => a.ActivityTime.CompareTo(b.ActivityTime));

            return Page();
        }

        private void FillOptions()
        {
            int currentYear = DateTime.Now.Year;
            YearOptions = new SelectList(new int[] { currentYear - 1, currentYear, currentYear + 1 });
            MonthOptions = new SelectList(Enumerable.Range(1, 12));
        }
    }
}