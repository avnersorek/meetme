﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Admin
{
    public class CitiesModel : PageModel
    {
        private readonly IDictionaryService _dictionaryService;
        private readonly IUserContextService _userContextService;

        [BindProperty]
        public string NewCityName { get; set; }

        public List<City> Cities { get; set; }

        public CitiesModel(IDictionaryService dictionaryService, IUserContextService userContextService)
        {
            _userContextService = userContextService;
            _dictionaryService = dictionaryService;
            Cities = _dictionaryService.GetCities();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid) return Page();

            User currentUser = _userContextService.GetLoggedInUser();
            if (currentUser.Role != Core.Models.User.RoleType.Admin) return Unauthorized();

            City newCity = new City() { Name = NewCityName };

            try
            {
                _dictionaryService.AddCity(newCity);
                // For page reload                
                Cities = _dictionaryService.GetCities();
            }
            catch (EntityAlreadyExistsException)
            {
                ModelState.AddModelError("NewCityName", "City already exists");
            }

            return Page();
        }
    }
}