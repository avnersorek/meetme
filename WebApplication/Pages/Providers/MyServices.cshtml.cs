﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Services;

namespace MeetMe.Web.Pages.Providers
{   
    public class MyServicesModel : PageModel
    {
        private readonly IUserContextService _userContextService;
        private readonly IProvidedServicesService _providedServicesService;
        private readonly IMeetingService _meetingService;
        
        public List<ProvidedServiceStatus> MyServices { get; set; }

        public MyServicesModel(
            IUserContextService userContextService, 
            IProvidedServicesService providedServicesService,
            IMeetingService meetingService
            )
        {
            _userContextService = userContextService;
            _providedServicesService = providedServicesService;
            _meetingService = meetingService;
        }

        public void OnGet()
        {
            int currentUserId = _userContextService.GetLoggedInUserId();
            MyServices = _providedServicesService.GetServiceStatusesForServiceProvider(currentUserId);
        }
    }
}