﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Utilities;
using MeetMe.Web.Resources;

namespace MeetMe.Web.Pages.Providers
{
    public class EditServiceModel : ProviderServiceBaseModel
    {
        public EditServiceModel(
            IProvidedServicesService providedServiceService,
            IStringLocalizer<ValidationResources> stringLocalizer)
            : base(providedServiceService, stringLocalizer)
        {            
        }

        public void OnGet(int serviceId)
        {
            ProvidedService = _providedServicesService.GetServiceById(serviceId);
        }

        public async Task<IActionResult> OnPostAsync(int serviceId)
        {            
            var uploadedImageSrc = await GetUploadedImageSource();

            if (!ValidateForm()) return Page();

            ProvidedService service = _providedServicesService.GetServiceById(serviceId);
            
            service.ServiceName = ProvidedService.ServiceName;
            service.PricePerMeeting = ProvidedService.PricePerMeeting;
            service.Hours = ProvidedService.Hours;
            service.Days = ProvidedService.Days;
            service.CityId = ProvidedService.CityId;
            service.ServiceTypeId = ProvidedService.ServiceTypeId;
            if (uploadedImageSrc != String.Empty) service.ImageSrc = uploadedImageSrc;

            _providedServicesService.UpdateService(service);

            return RedirectToPage(Routes.ProvidersMyServices);
        }
    }
}