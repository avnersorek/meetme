﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Resources;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Pages.Providers
{
    public class ProviderServiceBaseModel : PageModel
    {
        protected readonly IProvidedServicesService _providedServicesService;
        protected readonly IStringLocalizer<ValidationResources> _stringLocalizer;

        [BindProperty]
        public ProvidedService ProvidedService { get; set; }
        public IFormFile ImageFileUpload { get; set; }

        public ProviderServiceBaseModel(
            IProvidedServicesService providedServicesService,
            IStringLocalizer<ValidationResources> stringLocalizer)
        {
            _providedServicesService = providedServicesService;
            _stringLocalizer = stringLocalizer;
        }

        protected bool ValidateForm()
        {
            if (ProvidedService.PricePerMeeting == 0)
                ModelState.AddModelError("PricePerMeeting", _stringLocalizer.GetString("Price is required"));
            if (ProvidedService.Days == null || ProvidedService.Days.Count == 0)
                ModelState.AddModelError("Days", _stringLocalizer.GetString("Days are required"));
            if (ProvidedService.Hours == null || ProvidedService.Hours.Count == 0)
                ModelState.AddModelError("Hours", _stringLocalizer.GetString("Hours are required"));

            if (!ModelState.IsValid)
            {
                PageUtils.FixValidationErrors(ModelState, _stringLocalizer);
                return false;
            }

            return true;
        }

        protected async Task<string> GetUploadedImageSource()
        {
            return (ImageFileUpload == null) ? 
                String.Empty : 
                await FileHelpers.FormFileToByteArray(ImageFileUpload, ModelState);
        }
    }
}
