﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Providers.Reports
{
    public class MonthlyMeetingReportModel : PageModel
    {
        private readonly IProvidedServicesService _providedServicesService;
        private readonly IMeetingService _meetingService;
        private readonly IUserContextService _userContextService;

        public MonthlyMeetingReport Report { get; set; }        
        public ProvidedService ProvidedService { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }

        public SelectList YearOptions { get; set; }
        public SelectList MonthOptions { get; set; }

        public MonthlyMeetingReportModel(
            IProvidedServicesService providedServicesService,
            IMeetingService meetingService, 
            IUserContextService userContextService)
        {
            _providedServicesService = providedServicesService;
            _meetingService = meetingService;
            _userContextService = userContextService;

            FillOptions();
        }

        public IActionResult OnGet(int year, int month, int serviceId)
        {
            var providedService = _providedServicesService.GetServiceById(serviceId);
            var loggedInUserId = _userContextService.GetLoggedInUserId();

            if (providedService.ServiceProviderId != loggedInUserId) return Unauthorized();

            ProvidedService = providedService;
            Year = year;
            Month = month;
            Report = _meetingService.GetMonthlyMeetingReport(serviceId, year, month);

            return Page();
        }

        private void FillOptions()
        {
            int currentYear = DateTime.Now.Year;
            YearOptions = new SelectList(new int[] { currentYear - 1, currentYear, currentYear + 1 });
            MonthOptions = new SelectList(Enumerable.Range(1, 12));
        }
    }
}