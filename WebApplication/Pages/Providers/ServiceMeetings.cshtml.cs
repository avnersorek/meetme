﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Providers
{
    public class ServiceMeetingsModel : PageModel
    {
        private readonly IMeetingService _meetingService;
        private readonly IProvidedServicesService _providedServicesService;

        public string ServiceName { get; set; }
        public int ServiceId { get; set; }

        public List<(string, List<Meeting>)> MeetingsByCategory;

        public ServiceMeetingsModel(IMeetingService meetingService, IProvidedServicesService providedServicesService)
        {
            _meetingService = meetingService;
            _providedServicesService = providedServicesService;
        }

        public void OnGet(int serviceId)
        {
            ServiceId = serviceId;
            var service = _providedServicesService.GetServiceById(serviceId);
            ServiceName = service.ServiceName;

            var statusesToGet = new [] { Meeting.MeetingStatus.Scheduled, Meeting.MeetingStatus.Approved, Meeting.MeetingStatus.Paid };
            var meetings = _meetingService.GetMeetingsForService(serviceId, statusesToGet);

            MeetingsByCategory = new List<(string, List<Meeting>)>
            {
                ("New Meetings", meetings.FindAll(meeting => meeting.Status == Meeting.MeetingStatus.Scheduled)),
                ("Approved Meetings", meetings.FindAll(meeting => meeting.Status == Meeting.MeetingStatus.Approved)),
                ("Paid Meetings", meetings.FindAll(meeting => meeting.Status == Meeting.MeetingStatus.Paid))
            };
        }
    }
}