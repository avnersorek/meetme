﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

using MeetMe.Web.Utilities;
using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Resources;

namespace MeetMe.Web.Pages.Providers
{    
    public class AddServiceModel : ProviderServiceBaseModel
    {
       public AddServiceModel(
            IProvidedServicesService providedServicesService,
            IStringLocalizer<ValidationResources> stringLocalizer)
            : base(providedServicesService, stringLocalizer)
        {
            ProvidedService = new ProvidedService()
            {
                ImageSrc = _providedServicesService.GetServiceImagePlaceholder()
            };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ValidateForm()) return Page();
            ProvidedService.ImageSrc = await GetUploadedImageSource();
            _providedServicesService.AddService(ProvidedService);
            return RedirectToPage(Routes.ProvidersMyServices);
        }
    }
}