﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Web.Utilities;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Providers
{
    public class DeleteServiceModel : PageModel
    {
        private readonly IProvidedServicesService _providedServicesService;
        private readonly IUserContextService _userContextService;

        public ProvidedService ProvidedService { get; set; }

        public DeleteServiceModel(IProvidedServicesService providedServicesService, 
            IUserContextService userContextService)
        {
            _providedServicesService = providedServicesService;
            _userContextService = userContextService;
        }

        public IActionResult OnPost(int serviceId)
        {
            ProvidedService = _providedServicesService.GetServiceById(serviceId);
            var currentUserId = _userContextService.GetLoggedInUserId();

            if (ProvidedService.ServiceProvider.UserId != currentUserId) return Unauthorized();

            _providedServicesService.DeleteService(serviceId);

            return RedirectToPage(Routes.ProvidersMyServices);
        }

        public void OnGet(int serviceId)
        {
            ProvidedService = _providedServicesService.GetServiceById(serviceId);            
        }
    }
}