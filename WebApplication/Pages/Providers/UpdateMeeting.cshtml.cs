﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Pages.Providers
{
    public class UpdateMeetingModel : PageModel
    {
        private readonly IMeetingService _meetingService;
        private readonly IUserContextService _userContextService;

        public const string APPROVE_ACTION = "approve";
        public const string DECLINE_ACTION = "decline";

        public UpdateMeetingModel(IMeetingService meetingService, IUserContextService userContextService)
        {
            _meetingService = meetingService;
            _userContextService = userContextService;
        }

        public IActionResult OnGet(int meetingId, Meeting.MeetingStatus status)
        {
            Meeting meeting = _meetingService.GetMeetingById(meetingId);
            int loggedInUserId = _userContextService.GetLoggedInUserId();            

            if (meeting.ProvidedService.ServiceProvider.UserId != loggedInUserId) return Unauthorized();

            _meetingService.UpdateMeetingStatus(meetingId, status);

            return RedirectToPage(Routes.ProvidersServiceMeetings, new { serviceId = meeting.ProvidedService.ProvidedServiceId });
        }
    }
}