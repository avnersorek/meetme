﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;

namespace MeetMe.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly string newRoute;

        public IndexModel(IUserContextService userContextService)
        {
            newRoute = userContextService.GetRoutingStrategy().GetHomeRoute();            
        }

        public IActionResult OnGet()
        {
            return RedirectToPage(newRoute);
        }
    }
}
