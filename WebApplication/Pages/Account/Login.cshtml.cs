﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Resources;

namespace MeetMe.Web.Pages.Account
{
    public class LoginData
    {
        [Required(ErrorMessage="Email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage="Password is required"), DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }

    public class LoginModel : PageModel
    {
        private readonly IUserService _userService;
        private readonly IUserContextService _userContextService;
        private readonly IStringLocalizer<ValidationResources> _stringLocalizer;

        [BindProperty]
        public LoginData LoginData { get; set; }

        public LoginModel(IUserService userService, 
                          IUserContextService userContextService,
                          IStringLocalizer<ValidationResources> stringLocalizer)
        {
            _userService = userService;
            _userContextService = userContextService;
            _stringLocalizer = stringLocalizer;            
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) {
                return Page();
            }

            try {
                User loggedInUser = this._userService.VerifyPasswordAndGetUser(LoginData.Email, LoginData.Password);
                await _userContextService.LoginAsync(loggedInUser, LoginData.RememberMe);
                return RedirectToPage(Utilities.Routes.Home);
            }
            catch (AccountNotActiveException) {
                return FormError("Email", "Account Not Active");
            }
            catch (EntityNotFoundException) {
                return FormError("Email", "Email not found");
            }
            catch (WrongPasswordException) {
                return FormError("Password", "Wrong password");
            }
        }   

        private IActionResult FormError(string field, string message)
        {
            ModelState.AddModelError(field, _stringLocalizer.GetString(message));
            return Page();
        }
    }
}