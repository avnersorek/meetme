﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using Microsoft.Extensions.Localization;
using MeetMe.Web.Resources;

namespace MeetMe.Web.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly IUserService _userService;
        private readonly IUserContextService _userContextService;
        private readonly IStringLocalizer<ValidationResources> _stringLocalizer;

        public RegisterModel(IUserService userService, 
                             IUserContextService userContextService,
                             IStringLocalizer<ValidationResources> stringLocalizer)
        {
            _userService = userService;
            _userContextService = userContextService;
            _stringLocalizer = stringLocalizer;
        }

        [BindProperty]
        public RegisterRequest RegisterForm { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            try {
                var newUser = _userService.CreateUser(RegisterForm);
                await _userContextService.LoginAsync(newUser, true);
                return RedirectToPage(Utilities.Routes.Home);
            } 
            catch(EntityAlreadyExistsException) {
                return FormError("Email", "Account already exists");
            }
            catch(PasswordConfirmMismatchException) {
                return FormError("ConfirmPassword", "Passwords do not match");
            }
        }

        public IActionResult FormError(string field, string message)
        {
            ModelState.AddModelError(field, _stringLocalizer.GetString(message));
            return Page();
        }
    }
}