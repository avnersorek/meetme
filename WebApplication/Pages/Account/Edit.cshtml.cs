﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Account
{
    public class ChangePasswordForm
    {
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Password Confirmation is required")]
        [Compare("Password", ErrorMessage = "Password Confirmation is required")]
        public string ConfirmPassword { get; set; }
    }

    public class EditModel : PageModel
    {
        private readonly IUserService _userService;
        private readonly IUserContextService _userContextService;

        private const string USER_ID_QS_KEY = "userId";

        [BindProperty]
        public User EditedUser { get; set; }
        [BindProperty]
        public ChangePasswordForm ChangePassword { get; set; }
        [BindProperty]
        public int EditedUserId { get; set; }

        public bool ShowApprovedStatus { get; set; }
        public string ApprovedStatus { get; set; }
        public bool IsAdmin { get; set; }

        public EditModel(IUserService userService, IUserContextService userContextService)
        {
            this._userService = userService;
            this._userContextService = userContextService;            
        }

        private User GetUserToEdit()
        {
            User currentUser = this._userContextService.GetLoggedInUser();
            User userToEdit;
            IsAdmin = currentUser.Role == Core.Models.User.RoleType.Admin;
            bool requestHasUserId = HttpContext.Request.Query.ContainsKey(USER_ID_QS_KEY);

            if (IsAdmin && requestHasUserId)
            {
                int editedUserId = int.Parse(HttpContext.Request.Query[USER_ID_QS_KEY]);
                userToEdit = _userService.GetUserById(editedUserId);
            } else
            {
                userToEdit = currentUser;
            }

            ShowApprovedStatus = currentUser.Role == Core.Models.User.RoleType.ServiceProvider;
            ApprovedStatus = userToEdit.IsApproved ? "Approved" : "Not Approved";
            return userToEdit;
        }

        private void FilterPasswordFieldErrors(bool keepPasswordFields)
        {
            foreach (var field in ModelState)
            {
                if (field.Value.ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Invalid)
                {
                    bool isPasswordField = field.Key.Contains("Password") || field.Key.Contains("ConfirmPassword");
                    bool shouldClearErrors = (isPasswordField && !keepPasswordFields) || (!isPasswordField && keepPasswordFields);
                    if (shouldClearErrors)
                    {
                        field.Value.ValidationState = Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid;
                    }
                }
            }
        }

        public void OnGet()
        {
            EditedUser = GetUserToEdit();
            EditedUserId = EditedUser.UserId;
        }

        public IActionResult OnPostUpdateUserAsync()
        {
            FilterPasswordFieldErrors(false);
            if (!ModelState.IsValid) return Page();            
            EditedUser.UserId = EditedUserId;
            _userService.UpdateUser(EditedUser);
            return RedirectToPage(_userContextService.GetRoutingStrategy().GetHomeRoute());
        }

        public IActionResult OnPostChangePasswordAsync()
        {
            FilterPasswordFieldErrors(true);
            if (!ModelState.IsValid) return Page();            
            _userService.UpdatePassword(EditedUserId, ChangePassword.Password);
            return RedirectToPage(_userContextService.GetRoutingStrategy().GetHomeRoute());
        }

    }
}