﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using MeetMe.Core.Interfaces;

namespace MeetMe.Web.Pages.Account
{
    public class LogoutModel : PageModel
    {
        private readonly ILoginService _loginService;

        public LogoutModel(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await _loginService.LogoutAsync();
            return RedirectToPage(Utilities.Routes.Login);
        }
    }
}