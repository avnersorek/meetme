﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;

namespace MeetMe.Web.Pages.Components.AppHeader
{
    public class AppHeaderViewModel
    {
        public bool IsUserSignedIn { get; set; } 
        public string Email { get; set; }
        public List<(string, string)> MenuItems { get; set; }
    }

    public class AppHeaderViewComponent : ViewComponent
    {
        private readonly IUserContextService _userContextService;

        public AppHeaderViewComponent(IUserContextService userContextService)
        {
            _userContextService = userContextService;
        }

        public IViewComponentResult Invoke()
        {
            var model = new AppHeaderViewModel()
            {
                IsUserSignedIn = _userContextService.IsUserLoggedIn()
            };
                        
            if (model.IsUserSignedIn)
            {
                var user = _userContextService.GetLoggedInUser();

                model.Email = user.Email;
                model.MenuItems = _userContextService.GetRoutingStrategy().GetSiteMenuItems();
            }

            return View(this.GetType().Name, model);
        }
    }
}
