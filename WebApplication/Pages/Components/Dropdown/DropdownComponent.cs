﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MeetMe.Web.Pages.Components.Dropdown
{
    public class Dropdown : ViewComponent
    {
        public List<(int, string)> Options { get; set; }
        public List<int> PreselectedOptions { get; set; }
        public string Title { get; set; }
        public string TargetKey { get; set; }
        public bool IsMultiple { get; set; }

        public IViewComponentResult Invoke(Dropdown dropdown)
        {
            dropdown.PreselectedOptions = (dropdown.PreselectedOptions == null) ? new List<int>() : dropdown.PreselectedOptions;
            return View(this.GetType().Name, dropdown);
        }
    }
}
