﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Web.Pages.Components.Dropdown;

namespace MeetMe.Web.Pages.Components.ServiceTypeDropdown
{
    public class ServiceTypeDropdown : ViewComponent
    {
        private readonly IDictionaryService _dictionaryService;

        public ServiceTypeDropdown(IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService;
        }

        public IViewComponentResult Invoke(string targetKey, int? selectedId)
        {
            List<int> SelectedIds = (selectedId.HasValue) ? new List<int> { selectedId.Value } : new List<int>();

            Dropdown.Dropdown dropDown= new Dropdown.Dropdown()
            {
                Title = "Service Type",
                Options = _dictionaryService.GetServiceTypes().Select(serviceType => (ServiceTypeId: serviceType.ServiceTypeId, Name:serviceType.Name)).ToList(),
                TargetKey = targetKey,
                PreselectedOptions = SelectedIds
            };

            return View(this.GetType().Name, dropDown);
        }
    }
}
