﻿using MeetMe.Core.Interfaces;
using MeetMe.Web.Pages.Components.Dropdown;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeetMe.Web.Pages.Components.DayAndHourDropdown
{
    public class DayAndHourDropdown : ViewComponent
    {
        private readonly ISchedulingService _schedulingService;

        public Dropdown.Dropdown DayDropdown { get; set; }
        public Dropdown.Dropdown HourDropdown { get; set; }

        public DayAndHourDropdown(ISchedulingService schedulingService)
        {
            _schedulingService = schedulingService;
        }

        public IViewComponentResult Invoke(string daysTargetKey, string hoursTargetKey, List<int> selectedDays, List<int> selectedHours)
        {
            DayDropdown = new Dropdown.Dropdown()
            {
                Title = "Days",
                Options = _schedulingService.GetDays(),
                TargetKey = daysTargetKey,
                PreselectedOptions = selectedDays,
                IsMultiple = true
            };

            HourDropdown = new Dropdown.Dropdown()
            {
                Title = "Hours",
                Options = _schedulingService.GetHours(),
                TargetKey = hoursTargetKey,
                PreselectedOptions = selectedHours,
                IsMultiple = true
            };

            return View(this.GetType().Name, this);
        }

    }
}
