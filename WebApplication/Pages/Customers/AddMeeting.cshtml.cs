﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Customers
{
    public class AddMeetingModel : PageModel
    {
        private readonly IUserContextService _userContextService;
        private readonly ISchedulingService _schedulingService;
        private readonly IProvidedServicesService _providedServicesService;

        public ProvidedService ProvidedService { get; set; }
        public List<DateTime> Days { get; set; }
        public List<(string, ScheduleSlot[])> SlotsByHours { get; set; }

        private const int SCHEDULE_DAYS_TO_SHOW = 7;

        public AddMeetingModel(
            IUserContextService userContextService,
            ISchedulingService schedulingService,
            IProvidedServicesService providedServicesService
            )
        {
            _userContextService = userContextService;
            _schedulingService = schedulingService;
            _providedServicesService = providedServicesService;
        }

        public void OnGet()
        {
            int serviceId = int.Parse(HttpContext.Request.Query["serviceId"][0]);
            int customerId = _userContextService.GetLoggedInUserId();

            DateTime startTime = DateTime.Today.AddDays(1);
            DateTime endTime = startTime.AddDays(SCHEDULE_DAYS_TO_SHOW - 1);

            ProvidedService = _providedServicesService.GetServiceById(serviceId);
            Days = _schedulingService.GetDaysInRange(startTime, endTime);
            SlotsByHours = _schedulingService.GetScheduleForService(serviceId, customerId, startTime, endTime);            
        }
    }
}