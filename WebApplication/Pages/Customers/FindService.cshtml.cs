﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;

using MeetMe.Core.Models;
using MeetMe.Web.Utilities;
using MeetMe.Web.Resources;

namespace MeetMe.Web.Pages.Customers
{
    public class FindServiceModel : PageModel
    {
        private readonly IStringLocalizer<ValidationResources> _stringLocalizer;

        [BindProperty]
        public ServiceSearchRequest ServiceDetails { get; set; }

        public FindServiceModel(IStringLocalizer<ValidationResources> stringLocalizer)
        {
            _stringLocalizer = stringLocalizer;
        }
          
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                PageUtils.FixValidationErrors(ModelState, _stringLocalizer);
                return Page();
            }
            var queryArguments = PageUtils.ObjectToDictionary(ServiceDetails);
            return RedirectToPage(Routes.CustomersSearchResults, queryArguments);
        }
    }
}