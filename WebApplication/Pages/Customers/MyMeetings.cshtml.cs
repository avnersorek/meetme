﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Pages.Customers
{
    public class MyMeetingsModel : PageModel
    {
        private readonly IMeetingService _meetingService;
        private readonly IUserContextService _userContextService;

        public List<Meeting> FutureMeetings { get; set; }
        public List<Meeting> UnpaidMeetings { get; set; }
        public List<Meeting> PaidMeetings { get; set; }

        public MyMeetingsModel(IMeetingService meetingService,
            IUserContextService userContextService)
        {
            _meetingService = meetingService;
            _userContextService = userContextService;
        }

        public void OnGet()
        {
            int userId = _userContextService.GetLoggedInUserId();
            var meetings = _meetingService.GetMeetingsForCustomer(userId);
            FutureMeetings = meetings.Where(meeting => meeting.ScheduledTime > DateTime.Now).ToList();
            UnpaidMeetings = meetings.Where(meeting => 
                meeting.ScheduledTime <= DateTime.Now && 
                meeting.Status != Meeting.MeetingStatus.Cancelled && 
                meeting.Status != Meeting.MeetingStatus.Paid
            ).ToList();
            PaidMeetings = meetings.Where(meeting => meeting.Status == Meeting.MeetingStatus.Paid).ToList();
        }
    }
}