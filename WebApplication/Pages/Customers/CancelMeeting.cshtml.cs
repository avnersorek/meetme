﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Pages.Customers
{
    public class CancelMeetingModel : PageModel
    {
        private readonly IMeetingService _meetingService;
        private readonly IUserContextService _userContextService;

        public Meeting Meeting { get; set; }

        public CancelMeetingModel(IMeetingService meetingService, IUserContextService userContextService)
        {
            _meetingService = meetingService;
            _userContextService = userContextService;
        }

        public void OnGet(int meetingId)
        {
            Meeting = _meetingService.GetMeetingById(meetingId);
        }

        public IActionResult OnPost(int meetingId)
        {
            if (!ModelState.IsValid) return Page();

            int customerUserId = _userContextService.GetLoggedInUserId();
            var meeting = _meetingService.GetMeetingById(meetingId);

            if (meeting.Customer.UserId != customerUserId) return Unauthorized();

            _meetingService.UpdateMeetingStatus(meetingId, Meeting.MeetingStatus.Cancelled);

            return RedirectToPage(Routes.CustomersMyMeetings);
        }
    }
}