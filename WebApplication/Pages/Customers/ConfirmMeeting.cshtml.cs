﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Pages.Customers
{
    public class ConfirmMeetingModel : PageModel
    {
        private readonly IProvidedServicesService _providedServicesService;
        private readonly IUserContextService _userContextService;
        private readonly IMeetingService _meetingService;

        public ProvidedService ProvidedService { get; set; }
        public string MeetingDate { get; set; }
        public string MeetingHour { get; set; }

        public ConfirmMeetingModel(IProvidedServicesService providedServicesService,
            IMeetingService meetingService,
            IUserContextService userContextService)
        {
            _providedServicesService = providedServicesService;
            _meetingService = meetingService;
            _userContextService = userContextService;   
        }

        public void OnGet()
        {
            ParseQueryString();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid) return Page();

            ParseQueryString();            
            int customerUserId = _userContextService.GetLoggedInUserId();
            DateTime scheudledTime = DateTime.Parse(MeetingDate + " " + MeetingHour);

            _meetingService.AddMeeting(ProvidedService.ProvidedServiceId, customerUserId, scheudledTime);

            return RedirectToPage(Routes.CustomersMyMeetings);
        }

        public void ParseQueryString()
        {
            var queryString = HttpContext.Request.Query;

            var serviceId = int.Parse(queryString["serviceId"][0]);
            ProvidedService = _providedServicesService.GetServiceById(serviceId);

            MeetingDate = queryString["day"][0];

            MeetingHour = PageUtils.FormatHour(int.Parse(queryString["hour"][0]));
        }
    }
}