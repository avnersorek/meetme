﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Pages.Customers
{
    public class SearchResultsModel : PageModel
    {
        private IProvidedServicesService _providedServicesService;

        public List<ProvidedService> SearchResults { get; set; }

        public SearchResultsModel(IProvidedServicesService providedServicesService)
        {
            _providedServicesService = providedServicesService;
        }

        public void OnGet()
        {
            Dictionary<string, string> parameters = HttpContext.Request.Query.ToDictionary(o => o.Key, o => o.Value.ToString());
            ServiceSearchRequest searchDetails = PageUtils.DictionaryToObject<ServiceSearchRequest>(parameters);
            SearchResults = _providedServicesService.SearchServices(searchDetails);            
        }
    }
}