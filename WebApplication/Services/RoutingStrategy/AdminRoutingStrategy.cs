﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Services
{
    public class AdminRoutingStrategy : IRoutingStrategy
    {
        public string GetHomeRoute()
        {
            return Routes.AdminUsers;
        }

        public List<(string, string)> GetSiteMenuItems()
        {
            return new List<(string, string)>
            {
                (Routes.AdminUsers, "Users"),
                (Routes.AdminUserActivityReport, "User Activity Report"),
                (Routes.AdminServiceTypes, "Service Type List"),
                (Routes.AdminCities, "City List")
            };
        }
    }
}
