﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Services
{
    public class CustomerRoutingStrategy : IRoutingStrategy
    {
        public string GetHomeRoute()
        {
            return Routes.CustomersFindService;
        }

        public List<(string, string)> GetSiteMenuItems()
        {
            return new List<(string, string)> {
                (Routes.CustomersFindService, "Find a Service"),
                (Routes.CustomersMyMeetings, "My Meetings"),
                (Routes.MeetingsMySchedule, "My Schedule")
            };
        }
    }
}
