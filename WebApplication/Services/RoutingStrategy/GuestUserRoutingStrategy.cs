﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Services
{
    public class GuestUserRoutingStrategy : IRoutingStrategy
    {
        public string GetHomeRoute()
        {
            return Routes.Login;
        }

        public List<(string, string)> GetSiteMenuItems()
        {
            return new List<(string, string)>();
        }
    }
}
