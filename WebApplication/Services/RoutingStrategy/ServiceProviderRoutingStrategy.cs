﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Web.Utilities;

namespace MeetMe.Web.Services
{
    public class ServiceProviderRoutingStrategy : IRoutingStrategy
    {
        public string GetHomeRoute()
        {
            return Routes.ProvidersMyServices;
        }

        public List<(string, string)> GetSiteMenuItems()
        {
            return new List<(string, string)> {
                (Routes.ProvidersAddService, "Add Service"),
                (Routes.ProvidersMyServices, "My Services"),
                (Routes.MeetingsMySchedule, "My Schedule")
            };
        }
    }
}
