﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Web.Services
{
    public class UserActivityMiddleware
    {
        private readonly RequestDelegate _next;        

        public UserActivityMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        
        public async Task Invoke(HttpContext httpContext, IUserActivityService userActivityService, IUserContextService userContextService)
        {
            if (userContextService.IsUserLoggedIn())
            {
                int loggedInUserId = userContextService.GetLoggedInUserId();

                UserActivity activity = new UserActivity()
                {
                    UserId = loggedInUserId,
                    ActivityTime = DateTime.UtcNow,
                    Action = httpContext.Request.Method + " " + httpContext.Request.Path
                };

                userActivityService.AddActivity(activity);
            }

            await _next(httpContext);
        }
    }
}
