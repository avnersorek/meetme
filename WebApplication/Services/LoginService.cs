﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace MeetMe.Web.Services
{
    public class LoginService : ILoginService
    {
        private readonly HttpContext _httpContext;

        public LoginService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContext = httpContextAccessor.HttpContext;
        }

        public int GetLoggedInUserId()
        {
            return int.Parse(_httpContext.User.FindFirst(ClaimTypes.Sid).Value);
        }

        public bool IsUserLoggedIn()
        {
            return _httpContext.User.Identity.IsAuthenticated;
        }

        public async Task LoginAsync(User user, bool persist)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Sid, user.UserId.ToString()),
                new Claim(ClaimTypes.Role, user.Role.ToString())
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            await _httpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                new AuthenticationProperties { IsPersistent = persist }
            );
        }

        public Task LogoutAsync()
        {
            return _httpContext.SignOutAsync();
        }
    }
}
