﻿using System;
using System.Threading.Tasks;

using MeetMe.Core.Models;
using MeetMe.Core.Interfaces;

namespace MeetMe.Web.Services
{
    public class UserContextService : IUserContextService
    {        
        private readonly IUserService _userService;
        private readonly ILoginService _loginService;        

        public UserContextService(
            ILoginService loginService,
            IUserService userService
        ) {
            _loginService = loginService;
            _userService = userService;
        }

        public int GetLoggedInUserId() =>
            _loginService.GetLoggedInUserId();
        
        public Task LoginAsync(User user, bool persist) =>
            _loginService.LoginAsync(user, persist);

        public bool IsUserLoggedIn() =>
            _loginService.IsUserLoggedIn();

        public User GetLoggedInUser()
        {
            int currentUserId = GetLoggedInUserId();
            return this._userService.GetUserById(currentUserId);
        }

        public IRoutingStrategy GetRoutingStrategy()
        {
            User user = GetLoggedInUser();
            if (user == null) return new GuestUserRoutingStrategy();
            if (user.Role == User.RoleType.ServiceProvider) return new ServiceProviderRoutingStrategy();
            if (user.Role == User.RoleType.Customer) return new CustomerRoutingStrategy();
            if (user.Role == User.RoleType.Admin) return new AdminRoutingStrategy();
            else throw new Exception("unknown user role type");
        }

    }
}
