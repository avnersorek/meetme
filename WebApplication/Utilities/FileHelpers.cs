﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace MeetMe.Web.Utilities
{
    public class FileHelpers
    {
        private const int MAX_FILE_SIZE = 1048576; // 1 MB

        public static async Task<string> FormFileToByteArray(IFormFile formFile, ModelStateDictionary modelState)
        {
            var fileName = WebUtility.HtmlEncode(Path.GetFileName(formFile.FileName));

            if (!formFile.ContentType.ToLower().Contains("image"))
            {
                modelState.AddModelError(formFile.Name, $"The file ({fileName}) must be an image");
            }

            if (formFile.Length == 0)
            {
                modelState.AddModelError(formFile.Name, $"The image file ({fileName}) is empty.");
            }
            else if (formFile.Length > MAX_FILE_SIZE)
            {
                modelState.AddModelError(formFile.Name, $"The image file ({fileName}) exceeds 1 MB.");
            }
            else
            {
                try
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await formFile.CopyToAsync(memoryStream);
                        byte[] byteArray = memoryStream.ToArray();

                        if (byteArray.Length == 0) modelState.AddModelError(formFile.Name, $"The image file ({fileName}) is empty.");
                        else
                        {
                            var base64 = Convert.ToBase64String(byteArray);
                            var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
                            return imgSrc;
                        }
                    }
                }
                catch (Exception)
                {
                    modelState.AddModelError(formFile.Name, $"The image file ({fileName}) upload failed");
                }
            }

            return String.Empty;
        }
    }
}
