﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeetMe.Web.Utilities
{
    public class Routes
    {
        public const string Root = "/";
        public const string Home = "/Index";
        public const string Error = "/Error";
        
        public const string Account = "/Account";
        public const string Register = Account + "/Register";
        public const string Login = Account + "/Login";
        public const string Logout = Account + "/Logout";
        public const string EditAccount = Account + "/Edit";

        public const string Providers = "/Providers";
        public const string ProvidersMyServices = Providers + "/MyServices";
        public const string ProvidersAddService = Providers + "/AddService";
        public const string ProvidersEditService = Providers + "/EditService";
        public const string ProvidersDeleteService = Providers + "/DeleteService";
        public const string ProvidersServiceMeetings = Providers + "/ServiceMeetings";
        public const string ProvidersUpdateMeeting = Providers + "/UpdateMeeting";
        public const string ProvidersMonthlyMeetingReport = Providers + "/Reports/MonthlyMeetingReport";

        public const string Customers = "/Customers";
        public const string CustomersFindService = Customers + "/FindService";
        public const string CustomersSearchResults = Customers + "/SearchResults";
        public const string CustomersAddMeeting = Customers + "/AddMeeting";
        public const string CustomersConfirmMeeting = Customers + "/ConfirmMeeting";
        public const string CustomersCancelMeeting = Customers + "/CancelMeeting";
        public const string CustomersMyMeetings = Customers + "/MyMeetings";

        public const string Meetings = "/Meetings";
        public const string MeetingsInvoice = Meetings + "/Invoice";
        public const string MeetingsMySchedule = Meetings + "/MySchedule";

        public const string Admin = "/Admin";
        public const string AdminUsers = Admin + "/Users";
        public const string AdminServiceTypes = Admin + "/ServiceTypes";
        public const string AdminCities = Admin + "/Cities";
        public const string AdminUserActivityReport = Admin + "/UserActivityReport";
    }
}
