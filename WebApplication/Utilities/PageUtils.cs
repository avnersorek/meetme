﻿using MeetMe.Web.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MeetMe.Web.Utilities
{
    public static class PageUtils
    {
        public static string FormatHour(int hour)
        {
            return hour.ToString("00") + ":00";
        }

        public static Dictionary<string, string> ObjectToDictionary(object obj)
        {
            var dict = new Dictionary<string, string>();

            foreach (PropertyInfo propertyInfo in obj.GetType().GetProperties())
            {
                if (propertyInfo.CanRead)
                {
                    object propertyValue = propertyInfo.GetValue(obj, null);

                    if (propertyValue == null) { continue; }
                    else if (propertyInfo.PropertyType != typeof(string) && 
                        typeof(IEnumerable).IsAssignableFrom(propertyInfo.PropertyType))
                    {
                        IEnumerable collection = (IEnumerable)propertyValue;
                        List<string> innerValues = new List<string>();
                        foreach (object item in collection) {
                            innerValues.Add(item.ToString());
                        }

                        if (innerValues.Count > 0)
                            dict.Add(propertyInfo.Name, String.Join(',', innerValues));
                    }
                    else
                    {
                        dict.Add(propertyInfo.Name, propertyValue.ToString());
                    }
                }
            }

            return dict;
        }

        public static T DictionaryToObject<T>(Dictionary<string, string> dict)
        {
            T obj = (T)Activator.CreateInstance(typeof(T));
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo propertyInfo in properties)
            {
                if (dict.ContainsKey(propertyInfo.Name))
                {
                    var propType = propertyInfo.PropertyType;

                    if (!propertyInfo.CanWrite) { continue; }
                    else if (propType == typeof(int[]))
                    {
                        int[] intArr = dict[propertyInfo.Name].Split(',').Select(s => int.Parse(s)).ToArray();
                        propertyInfo.SetValue(obj, intArr);
                    }
                    else if (propType == typeof(List<int>))
                    {
                        List<int> intList = dict[propertyInfo.Name].Split(',').Select(s => int.Parse(s)).ToList();
                        propertyInfo.SetValue(obj, intList);
                    }
                    else if (propType == typeof(int) || propType == typeof(int?))
                    {                        
                        propertyInfo.SetValue(obj, int.Parse(dict[propertyInfo.Name]));
                    }
                    else if (propType == typeof(string))
                    {
                        propertyInfo.SetValue(obj, dict[propertyInfo.Name]);
                    }

                }
            }

            return obj;
        }

        public static void FixValidationErrors(ModelStateDictionary modelState, IStringLocalizer<ValidationResources> stringLocalizer)
        {
            foreach (var field in modelState)
            {
                if (field.Value.ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Invalid)
                {
                    if (field.Key.EndsWith("CityId"))
                    {
                        field.Value.Errors.Clear();
                        modelState.AddModelError("CityId", stringLocalizer.GetString("City is required"));
                    }
                    else if (field.Key.EndsWith("ServiceTypeId"))
                    {
                        field.Value.Errors.Clear();
                        modelState.AddModelError("ServiceTypeId", stringLocalizer.GetString("Service Type is required"));
                    }
                }
            }
        }

    }
}
