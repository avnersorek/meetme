﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MeetMe.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace MeetMe.DB.DbProviders
{
    public class SqlCityDb : SqlDbBase<City>
    {
        public SqlCityDb(MeetMeDbContext context) : base(context) { }

        protected override DbSet<City> GetSet()
        {
            return context.Cities;
        }

        protected override IQueryable<City> GetSetForQuery()
        {
            return context.Cities;
        }
    }
}
