﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MeetMe.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace MeetMe.DB.DbProviders
{
    public class SqlUserActivityDb : SqlDbBase<UserActivity>
    {
        public SqlUserActivityDb(MeetMeDbContext context) : base(context) { }

        protected override DbSet<UserActivity> GetSet()
        {
            return context.UserActivities;
        }

        protected override IQueryable<UserActivity> GetSetForQuery()
        {
            return context.UserActivities
                .Include(activity => activity.User);
        }
    }
}
