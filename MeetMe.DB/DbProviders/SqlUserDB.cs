﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace MeetMe.DB.DbProviders
{
    public class SqlUserDB : SqlDbBase<User>
    {
        public SqlUserDB(MeetMeDbContext context) : base(context) { }

        protected override DbSet<User> GetSet()
        {
            return context.Users;
        }

        protected override IQueryable<User> GetSetForQuery()
        {
            return context.Users;
        }
    }
}
