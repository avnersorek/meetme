﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

using MeetMe.Core.Models;
using System.Linq;

namespace MeetMe.DB.DbProviders
{
    public class SqlServiceTypeDb : SqlDbBase<ServiceType>
    {
        public SqlServiceTypeDb(MeetMeDbContext context) : base(context) { }

        protected override DbSet<ServiceType> GetSet()
        {
            return context.ServiceTypes;
        }

        protected override IQueryable<ServiceType> GetSetForQuery()
        {
            return context.ServiceTypes;
        }
    }
}
