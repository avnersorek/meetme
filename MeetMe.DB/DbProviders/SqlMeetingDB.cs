﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace MeetMe.DB.DbProviders
{
    public class SqlMeetingDB : SqlDbBase<Meeting>
    {
        public SqlMeetingDB(MeetMeDbContext context) : base(context) { }

        protected override DbSet<Meeting> GetSet()
        {
            return context.Meetings;
        }

        protected override IQueryable<Meeting> GetSetForQuery()
        {
            return context
                    .Meetings
                    .Include(meeting => meeting.Customer)
                    .Include(meeting => meeting.ProvidedService)
                        .ThenInclude(providedService => providedService.ServiceProvider);
        }
    }
}
