﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace MeetMe.DB.DbProviders
{
    public class SqlProvidedServicesDB : SqlDbBase<ProvidedService>
    {
        public SqlProvidedServicesDB(MeetMeDbContext context) : base(context) { }

        protected override DbSet<ProvidedService> GetSet()
        {
            return context.ProvidedServices;
        }

        protected override IQueryable<ProvidedService> GetSetForQuery()
        {
            return context
                .ProvidedServices
                .Include(service => service.City)
                .Include(service => service.ServiceType)
                .Include(service => service.ServiceProvider);
        }
    }
}
