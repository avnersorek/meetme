﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace MeetMe.DB.DbProviders
{
    public abstract class SqlDbBase<T> : IDbProvider<T> where T : class
    {
        protected readonly MeetMeDbContext context;

        public SqlDbBase(MeetMeDbContext newContext)
        {
            context = newContext;            
        }

        public T Add(T newEntity)
        {
            GetSet().Add(newEntity);
            SaveChanges();
            return newEntity;
        }

        public T Find(Func<T, bool> predicate)
        {
            return GetSetForQuery().SingleOrDefault(predicate);
        }

        public List<T> FindAll(Func<T, bool> predicate)
        {
            return GetSetForQuery().Where(predicate).ToList();
        }

        public T Update(T updatedEntity)
        {
            SaveChanges();
            return updatedEntity;
        }

        private void SaveChanges()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException ex) when ((ex.InnerException as SqlException)?.Number == 2601)
            {
                throw new EntityAlreadyExistsException();
            }
        }

        protected abstract DbSet<T> GetSet();
        protected abstract IQueryable<T> GetSetForQuery();
    }
}
