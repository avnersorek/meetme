﻿using Microsoft.Extensions.DependencyInjection;

using MeetMe.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using MeetMe.Core.Models;

namespace MeetMe.DB
{
    public class ApplicationStartup
    {
        private const string CONNECTION_STRING= @"Server=(localdb)\mssqllocaldb;Database=MeetMeDb;Trusted_Connection=True;";

        public static IServiceCollection InjectServices(IServiceCollection services)
        {
            services.AddDbContext<MeetMeDbContext>(options => options.UseSqlServer(CONNECTION_STRING));

            services.AddScoped<IDbProvider<User>, DbProviders.SqlUserDB>();
            services.AddScoped<IDbProvider<ProvidedService>, DbProviders.SqlProvidedServicesDB>();
            services.AddScoped<IDbProvider<Meeting>, DbProviders.SqlMeetingDB>();
            services.AddScoped<IDbProvider<City>, DbProviders.SqlCityDb>();
            services.AddScoped<IDbProvider<ServiceType>, DbProviders.SqlServiceTypeDb>();
            services.AddScoped<IDbProvider<UserActivity>, DbProviders.SqlUserActivityDb>();

            return services;
        }
    }
}
