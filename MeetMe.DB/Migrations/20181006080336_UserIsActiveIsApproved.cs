﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeetMe.DB.Migrations
{
    public partial class UserIsActiveIsApproved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                column: "HashedPassword",
                value: "$MYHASH$V1$1000$CCjh1rHqiA3KFtz7xjeVUg7yLlZWMSRxDxB52jNDwZox+zuH");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Users");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                column: "HashedPassword",
                value: "$MYHASH$V1$1000$Vcng6oMz+5UqUTghwfuumfV4q2EITWcYnxDkGRrmVMTMZxOK");
        }
    }
}
