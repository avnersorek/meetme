﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeetMe.DB.Migrations
{
    public partial class RemoveMeetingUniqueIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProvidedServices_ServiceProviderId_ServiceName",
                table: "ProvidedServices");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_ProvidedServiceId_ScheduledTime",
                table: "Meetings");

            migrationBuilder.AlterColumn<string>(
                name: "ServiceName",
                table: "ProvidedServices",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                column: "HashedPassword",
                value: "$MYHASH$V1$1000$AbP8BRdzAH/GCE2U/facHCoy62TSA7AtAwD6sHS50xh1c09d");

            migrationBuilder.CreateIndex(
                name: "IX_ProvidedServices_ServiceProviderId_ServiceName",
                table: "ProvidedServices",
                columns: new[] { "ServiceProviderId", "ServiceName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_ProvidedServiceId",
                table: "Meetings",
                column: "ProvidedServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProvidedServices_ServiceProviderId_ServiceName",
                table: "ProvidedServices");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_ProvidedServiceId",
                table: "Meetings");

            migrationBuilder.AlterColumn<string>(
                name: "ServiceName",
                table: "ProvidedServices",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                column: "HashedPassword",
                value: "$MYHASH$V1$1000$9Rqh5cTS1XMScVMdDCjjCXRvO6jP0o3Mbkh7EeeNF0N/uUrC");

            migrationBuilder.CreateIndex(
                name: "IX_ProvidedServices_ServiceProviderId_ServiceName",
                table: "ProvidedServices",
                columns: new[] { "ServiceProviderId", "ServiceName" },
                unique: true,
                filter: "[ServiceName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_ProvidedServiceId_ScheduledTime",
                table: "Meetings",
                columns: new[] { "ProvidedServiceId", "ScheduledTime" },
                unique: true);
        }
    }
}
