﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeetMe.DB.Migrations
{
    public partial class AddAdminUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "HashedPassword", "Name", "Phone", "Role" },
                values: new object[] { 1, "admin@meet.me", "$MYHASH$V1$1000$Vcng6oMz+5UqUTghwfuumfV4q2EITWcYnxDkGRrmVMTMZxOK", "Administrator", null, 0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1);
        }
    }
}
