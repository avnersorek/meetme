﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

using MeetMe.Core.Models;
using MeetMe.Core.Interfaces;
using MeetMe.Core.Utilities;

namespace MeetMe.DB
{
    public class MeetMeDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ProvidedService> ProvidedServices { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }

        public MeetMeDbContext(DbContextOptions<MeetMeDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            ApplyIndexes(modelBuilder);
            ApplySeedData(modelBuilder);
        }

        private void ApplyIndexes(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<ProvidedService>()
                .HasIndex(s => new { s.ServiceProviderId, s.ServiceName })
                .IsUnique();

            modelBuilder.Entity<City>()
                .HasIndex(c => c.Name)
                .IsUnique();

            modelBuilder.Entity<ServiceType>()
                .HasIndex(c => c.Name)
                .IsUnique();
        }

        private void ApplySeedData(ModelBuilder modelBuilder)
        {
            IPasswordHasher passwordHasher = new RNGPasswordHasher();

            modelBuilder.Entity<User>().HasData(
                new User()
                {
                    UserId = 1,
                    Name = "Administrator",
                    Email = "admin@meet.me",
                    Role = User.RoleType.Admin,
                    HashedPassword = passwordHasher.Hash("admin"),
                    Phone = "123456"
                }
            );
        }
    }
}
