﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;
using static MeetMe.Core.Models.ScheduleSlot;

namespace MeetMe.Core.Services
{
    public class SchedulingService : ISchedulingService
    {        
        private readonly IDbProvider<Meeting> _meetingDb;
        private readonly IDbProvider<User> _userDb;
        private readonly IDbProvider<ProvidedService> _providedServiceDb;

        public SchedulingService(
            IDbProvider<Meeting> meetingDb, 
            IDbProvider<User> userDB,
            IDbProvider<ProvidedService> providedServiceDb)
        {
            _meetingDb = meetingDb;
            _userDb = userDB;
            _providedServiceDb = providedServiceDb;
        }

        public List<DateTime> GetDaysInRange(DateTime startTime, DateTime endTime)
        {
            List<DateTime> days = new List<DateTime>();
            for (var day = startTime; day.Date <= endTime.Date; day = day.AddDays(1))
                days.Add(day);
            return days;
        }

        public List<(string, ScheduleSlot[])> GetScheduleForService(int providedServiced, int customerId, DateTime startTime, DateTime endTime)
        {
            ProvidedService providedService = _providedServiceDb.Find(s => s.ProvidedServiceId == providedServiced);
            Func<Meeting, bool> isMeetingInTimeAndActive = 
                meeting => meeting.ScheduledTime >= startTime && meeting.ScheduledTime <= endTime &&
                meeting.Status != Meeting.MeetingStatus.Cancelled;

            List<Meeting> serviceMeetings = _meetingDb
                .FindAll(meeting => meeting.ProvidedServiceId == providedService.ProvidedServiceId && isMeetingInTimeAndActive(meeting));
            List<Meeting> customerMeetings = _meetingDb
                .FindAll(meeting => meeting.CustomerId == customerId && isMeetingInTimeAndActive(meeting));
            List<DateTime> possibleDays = GetDaysInRange(startTime, endTime);
            List<(int, string)> possibleHours = GetHours();

            return possibleHours.Select(hour =>
            {
                ScheduleSlot[] slots = possibleDays.Select(day =>
                {
                    DateTime slotTime = day.AddHours(hour.Item1);
                    ScheduleSlot newSlot = new ScheduleSlot()
                    {
                        Day = day,
                        Hour = hour.Item1
                    };

                    Meeting meetingByCustomer = customerMeetings.Find(m => m.ScheduledTime == slotTime);
                    Meeting meetingForService = serviceMeetings.Find(m => m.ScheduledTime == slotTime);
                    bool isServiceProvidedAtDayAndHour = providedService.Hours.Contains(hour.Item1) &&
                        providedService.Days.Contains((int)day.DayOfWeek + 1);

                    if (meetingByCustomer != null)
                    {
                        newSlot.Meetings = new List<Meeting> { meetingByCustomer };
                        newSlot.Status = ScheduleStatus.CustomerIsBusy;
                    } else if (!isServiceProvidedAtDayAndHour)
                    {
                        newSlot.Status = ScheduleStatus.Closed;
                    } else if (meetingForService != null)
                    {
                        newSlot.Status = ScheduleStatus.ServiceIsBusy;
                    } else
                    {
                        newSlot.Status = ScheduleStatus.Available;
                    }

                    return newSlot;
                }).ToArray();

                return (hour.Item2, slots);
            }).ToList();
        }

        public List<(string, ScheduleSlot[])> GetScheduleForUser(int userId, DateTime startTime, DateTime endTime)
        {
            User user = _userDb.Find(u => u.UserId == userId);
            List<Meeting> meetings = new List<Meeting>();
            if (user.Role == User.RoleType.Customer)
            {
                meetings = _meetingDb.FindAll(m => m.CustomerId == userId &&
                                                    m.ScheduledTime >= startTime && m.ScheduledTime <= endTime &&
                                                    m.Status != Meeting.MeetingStatus.Cancelled);
            }
            else if (user.Role == User.RoleType.ServiceProvider) {
                meetings = _meetingDb.FindAll(m => m.ProvidedService.ServiceProviderId == userId &&
                                                    m.ScheduledTime >= startTime && m.ScheduledTime <= endTime &&
                                                    m.Status != Meeting.MeetingStatus.Cancelled);
            }
            List<DateTime> possibleDays = GetDaysInRange(startTime, endTime);
            List<(int, string)> possibleHours = GetHours();

            return possibleHours.Select(hour =>
            {
                ScheduleSlot[] slots = possibleDays.Select(day =>
                {
                    DateTime slotTime = day.AddHours(hour.Item1);
                    List<Meeting> meetingsInSlot = meetings.FindAll(m => m.ScheduledTime == slotTime);

                    return new ScheduleSlot()
                    {
                        Day = day,
                        Hour = hour.Item1,
                        Meetings = meetingsInSlot
                    };
                }).ToArray();

                return (hour.Item2, slots);
            }).ToList();
        }

        public List<(int, string)> GetDays()
        {
            return new List<(int, string)>
            {
                (1, "Sunday"),
                (2, "Monday"),
                (3, "Tuesday"),
                (4, "Wednesday"),
                (5, "Thursday"),
                (6, "Friday"),
                (7, "Saturday")
            };
        }

        public List<(int, string)> GetHours()
        {
            return Enumerable.Range(8, 13).Select(i => (i, i.ToString("00") + ":00")).ToList();
        }
    }
}
