﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Core.Services
{
    public class MeetingService : IMeetingService
    {
        private readonly IDbProvider<Meeting> _meetingDB;
        private readonly IDbProvider<ProvidedService> _providedServicesDB;
        private readonly IDbProvider<User> _userDb;

        public MeetingService(IDbProvider<Meeting> meetingDb, 
                                IDbProvider<ProvidedService> providedServicesDB,
                                IDbProvider<User> userDb
            )
        {
            _meetingDB = meetingDb;
            _providedServicesDB = providedServicesDB;
            _userDb = userDb;
        }

        public Meeting AddMeeting(int providedServiceId, int customerUserId, DateTime scheduledTime)
        {
            ProvidedService providedService = _providedServicesDB.Find(ps => ps.ProvidedServiceId == providedServiceId);
            User customer = _userDb.Find(user => user.UserId == customerUserId);

            if (providedService == null || customer == null) throw new EntityNotFoundException();

            Meeting newMeeting = new Meeting()
            {
                ProvidedServiceId = providedServiceId,
                ProvidedService = providedService,
                CustomerId = customerUserId,
                Customer = customer,
                ScheduledTime = scheduledTime
            };

            _meetingDB.Add(newMeeting);

            return newMeeting;
        }
        
        public Meeting GetMeetingById(int meetingId)
            => _meetingDB.Find(meeting => meeting.MeetingId == meetingId);

        public List<Meeting> GetMeetingsForCustomer(int customerUserId)
            => _meetingDB.FindAll(meeting => meeting.Customer.UserId == customerUserId);

        public List<Meeting> GetMeetingsForService(int providedServiceId, Meeting.MeetingStatus[] statuses)
            => _meetingDB.FindAll(meeting =>
                    meeting.ProvidedServiceId == providedServiceId && statuses.Contains(meeting.Status));

        public void UpdateMeetingStatus(int meetingId, Meeting.MeetingStatus newStatus)
        {
            Meeting meetingToUpdate = GetMeetingById(meetingId);
            meetingToUpdate.Status = newStatus;
            _meetingDB.Update(meetingToUpdate);
        }

        public List<Meeting> GetMeetingsForService(int providedServiceId, DateTime startTime, DateTime endTime)
        {
            return _meetingDB.FindAll(meeting =>
                    meeting.ProvidedServiceId == providedServiceId &&
                    meeting.ScheduledTime > startTime &&
                    meeting.ScheduledTime < endTime
                );
        }

        public MonthlyMeetingReport GetMonthlyMeetingReport(int providedServiceId, int year, int month)
        {
            MonthlyMeetingReport report = new MonthlyMeetingReport
            {
                ReportStart = new DateTime(year, month, 1),
                ReportEnd = new DateTime(year, month, 1).AddMonths(1).AddSeconds(-1),
                MeetingsByStatus = new Dictionary<Meeting.MeetingStatus, List<Meeting>>(),
                TotalRevenue = 0,
                ActualRevenue = 0
            };

            var meetings = GetMeetingsForService(providedServiceId, report.ReportStart, report.ReportEnd);
            var uniqueCustomers = new HashSet<int>();

            foreach (Meeting meeting in meetings)
            {
                if (report.MeetingsByStatus.ContainsKey(meeting.Status)) report.MeetingsByStatus[meeting.Status].Add(meeting);
                else report.MeetingsByStatus[meeting.Status] = new List<Meeting> { meeting };

                if (meeting.Status != Meeting.MeetingStatus.Cancelled) report.TotalRevenue += meeting.ProvidedService.PricePerMeeting;
                if (meeting.Status == Meeting.MeetingStatus.Paid) report.ActualRevenue += meeting.ProvidedService.PricePerMeeting;

                uniqueCustomers.Add(meeting.Customer.UserId);
            }

            report.UniqueCustomerCount = uniqueCustomers.Count;
            report.TotalMeetingCount = meetings.Count;
            return report;
        }
    }
}
