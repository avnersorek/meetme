﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Core.Services
{
    public class ProvidedServicesService : IProvidedServicesService
    {
        private readonly IDbProvider<ProvidedService> _providedServicesDB;
        private readonly IDbProvider<User> _userDB;
        private readonly IDbProvider<Meeting> _meetingDB;
        private readonly IUserContextService _userContextService;

        private const string PLACEHOLDER_IMAGE = "/images/ImagePlaceholder.PNG";

        public ProvidedServicesService(IDbProvider<ProvidedService> providedServicesDB,
                                        IDbProvider<User> userDB,
                                        IDbProvider<Meeting> meetingDB,
                                        IUserContextService userContextService)
        {
            _providedServicesDB = providedServicesDB;
            _userDB = userDB;
            _meetingDB = meetingDB;
            _userContextService = userContextService;
        }

        public ProvidedService UpdateService(ProvidedService updatedService)
        {
            int currentUserId = _userContextService.GetLoggedInUserId();
            if (currentUserId != updatedService.ServiceProviderId)
                throw new UnauthorizedResourceException();

            _providedServicesDB.Update(updatedService);
            return updatedService;
        }

        public List<ProvidedService> SearchServices(ServiceSearchRequest search)
        {
            bool SearchPredicate(ProvidedService s)
            {
                return (search.ServiceTypeId == s.ServiceType.ServiceTypeId) &&
                    (search.CityId == s.City.CityId) &&
                    (search.MaxPricePerMeeting == null || search.MaxPricePerMeeting >= s.PricePerMeeting) &&                    
                    (search.Days == null ||
                        (search.Days.Count > 0 && s.Days.Any(day => search.Days.Contains(day)))) &&
                    (search.Hours == null ||
                        (search.Hours.Count > 0 && s.Hours.Any(hour => search.Hours.Contains(hour))) &&
                    (s.ServiceProvider.IsApproved && s.ServiceProvider.IsActive)
                );
            }

            return _providedServicesDB.FindAll(SearchPredicate);
        }

        public ProvidedService GetServiceById(int providedServiceId)
        {
            return _providedServicesDB.Find(service => service.ProvidedServiceId == providedServiceId);
        }

        public void DeleteService(int serviceId)
        {
            ProvidedService service = GetServiceById(serviceId);
            service.IsActive = false;
            _providedServicesDB.Update(service);
        }

        public ProvidedService AddService(ProvidedService newService)
        {
            User serviceProvider = _userContextService.GetLoggedInUser();

            if (serviceProvider == null) throw new EntityNotFoundException();
            if (serviceProvider.Role != User.RoleType.ServiceProvider) throw new UnauthorizedRoleException();

            newService.ServiceProviderId = serviceProvider.UserId;

            if (String.IsNullOrEmpty(newService.ImageSrc)) newService.ImageSrc = PLACEHOLDER_IMAGE;

            return _providedServicesDB.Add(newService);
        }

        public List<ProvidedServiceStatus> GetServiceStatusesForServiceProvider(int serviceProviderUserId)
        {
            var services = _providedServicesDB.FindAll(s => s.ServiceProviderId == serviceProviderUserId);
            var statuses = new Meeting.MeetingStatus[] { Meeting.MeetingStatus.Approved, Meeting.MeetingStatus.Scheduled };

            return services.Select(service =>
            {
                var serviceStatus = new ProvidedServiceStatus(service);
                var meetings = _meetingDB.FindAll(meeting =>
                    meeting.ProvidedServiceId == service.ProvidedServiceId && 
                    statuses.Contains(meeting.Status));

                foreach (var meeting in meetings)
                {
                    if (meeting.ScheduledTime > DateTime.Now && meeting.Status == Meeting.MeetingStatus.Approved)
                        serviceStatus.ApprovedMeetingsCount++;
                    else if (meeting.ScheduledTime > DateTime.Now && meeting.Status == Meeting.MeetingStatus.Scheduled)
                        serviceStatus.NewMeetingsCount++;
                    else
                        serviceStatus.UnpaidMeetingsCount++;
                }
                return serviceStatus;
            })
            .Where(service => service.IsActive || service.HasMeetings)
            .ToList();
        }      

        public string GetServiceImagePlaceholder()
        {
            return PLACEHOLDER_IMAGE;
        }
    }
}
