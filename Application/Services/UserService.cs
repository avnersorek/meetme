﻿using System;
using System.Collections.Generic;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IDbProvider<User> _userDB;
        private readonly IPasswordHasher _passwordHasher;
        private readonly ILoginService _loginService;

        public UserService(
            IDbProvider<User> userDB, 
            IPasswordHasher passwordHasher,
            ILoginService loginService)
        {
            _userDB = userDB;
            _passwordHasher = passwordHasher;
            _loginService = loginService;
        }

        public User GetUserById(int id)
            => _userDB.Find(user => user.UserId == id);

        public List<User> GetUsers()
            => _userDB.FindAll(user => user.Role != User.RoleType.Admin);

        public User UpdateUser(User changedUser)
        {
            int loggedInUserId = _loginService.GetLoggedInUserId();
            User loggedInUser = GetUserById(loggedInUserId);
            bool isAdminLoggedIn = loggedInUser.Role == User.RoleType.Admin;

            if (changedUser.UserId != loggedInUser.UserId && !isAdminLoggedIn)
                throw new UnauthorizedResourceException();

            User dbUser = _userDB.Find(user => user.UserId == changedUser.UserId);
            dbUser.Email = changedUser.Email;
            dbUser.Name = changedUser.Name;
            dbUser.Phone = changedUser.Phone;
            if (isAdminLoggedIn)
            {
                dbUser.IsApproved = changedUser.IsApproved;
                dbUser.IsActive = changedUser.IsActive;
            }
            return _userDB.Update(dbUser);
        }

        public User CreateUser(RegisterRequest request)
        {
            if (!request.Password.Equals(request.ConfirmPassword))
                throw new PasswordConfirmMismatchException();

            String hashedPassword = _passwordHasher.Hash(request.Password);
            
            User newUser = new User()
            {
                Email = request.Email,
                HashedPassword = hashedPassword,
                Role = request.IsServiceProvider? User.RoleType.ServiceProvider : User.RoleType.Customer,
                Name = request.Name,
                Phone = request.Phone,
                IsActive = true,
                IsApproved = false
            };

            return this._userDB.Add(newUser);
        }

        public void UpdatePassword(int customerId, string newPassword)
        {
            int loggedInUserId = _loginService.GetLoggedInUserId();
            User loggedInUser = _userDB.Find(user => user.UserId == loggedInUserId);
            User userToEdit = loggedInUser;

            if (loggedInUserId != customerId)
            {
                if (loggedInUser.Role == User.RoleType.Admin) // For admin changing user's password
                    userToEdit = _userDB.Find(user => user.UserId == customerId);
                else
                    throw new UnauthorizedResourceException();
            }
            
            userToEdit.HashedPassword = _passwordHasher.Hash(newPassword);
            _userDB.Update(userToEdit);
        }

        public User VerifyPasswordAndGetUser(string email, string password)
        {
            User user = this._userDB.Find(u => u.Email == email);
            if (user == null) {
                throw new EntityNotFoundException();
            }
            else if (!this._passwordHasher.Verify(password, user.HashedPassword)) {
                throw new WrongPasswordException();
            }
            else if (!user.IsActive)
            {
                throw new AccountNotActiveException();
            }
            else {
                return user;
            }
        }
    }
}
