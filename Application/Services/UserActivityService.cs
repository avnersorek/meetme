﻿using System;
using System.Collections.Generic;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Core.Services
{
    public class UserActivityService : IUserActivityService
    {
        private readonly IDbProvider<UserActivity> _userActivityDb;

        public UserActivityService(IDbProvider<UserActivity> userActivityDb)
        {
            _userActivityDb = userActivityDb;
        }

        public void AddActivity(UserActivity newActivity)
            => _userActivityDb.Add(newActivity);

        public List<UserActivity> GetActivityForMonth(int year, int month)
        {
            DateTime startTime = new DateTime(year, month, 1);
            DateTime endTime = startTime.AddMonths(1);

            return _userActivityDb.FindAll(activity => activity.ActivityTime >= startTime &&
                                                       activity.ActivityTime < endTime);
        }
    }
}
