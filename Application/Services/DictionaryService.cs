﻿using System;
using System.Collections.Generic;
using System.Text;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Models;

namespace MeetMe.Core.Services
{
    public class DictionaryService : IDictionaryService
    {
        private readonly IDbProvider<City> _cityDb;
        private readonly IDbProvider<ServiceType> _serviceTypeDb;

        public DictionaryService(IDbProvider<City> cityDb, IDbProvider<ServiceType> serviceTypeDb)
        {
            _cityDb = cityDb;
            _serviceTypeDb = serviceTypeDb;
        }

        public void AddCity(City newCity)
            => _cityDb.Add(newCity);

        public void AddServiceType(ServiceType newServiceType)
            => _serviceTypeDb.Add(newServiceType);

        public List<City> GetCities()
            => _cityDb.FindAll(_ => true);

        public List<ServiceType> GetServiceTypes()
            => _serviceTypeDb.FindAll(_ => true);
    }
}
