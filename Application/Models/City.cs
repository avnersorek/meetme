﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Models
{
    public class City
    {
        public int CityId { get; set; }
        public string Name { get; set; }
    }
}
