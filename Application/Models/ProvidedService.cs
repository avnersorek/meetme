﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MeetMe.Core.Models
{
    public class ProvidedService 
    {
        public int ProvidedServiceId { get; set;  }
        public string ImageSrc { get; set; }
        public bool IsActive { get; set; }

        [Required(ErrorMessage ="Name is required")]
        public string ServiceName { get; set; }        

        [Required(ErrorMessage ="Price is required")]
        public int PricePerMeeting { get; set; }        

        [Required(ErrorMessage = "Service Provider is required")]
        public int ServiceProviderId { get; set; }
        public User ServiceProvider { get; set; }

        [Required(ErrorMessage = "Service Type is required")]
        public int ServiceTypeId { get; set; }
        public ServiceType ServiceType { get; set; }

        [Required(ErrorMessage = "City is required")]
        public int CityId { get; set; }
        public City City { get; set; }

        public string DaysString { get; set; }
        [NotMapped]
        public List<int> Days {
            get
            {
                if (DaysString == null) return new List<int>();
                return DaysString.Split(',').Select(s => int.Parse(s)).ToList();
            }
            set
            {
                DaysString = String.Join(',', value);
            }
        }

        public string HoursString { get; set; }
        [NotMapped]
        public List<int> Hours {
            get
            {
                if (HoursString == null) return new List<int>();
                return HoursString.Split(',').Select(s => int.Parse(s)).ToList();
            }
            set
            {
                var twoDigitHours = value.Select(hour => hour.ToString("00"));
                HoursString = String.Join(',', twoDigitHours);
            }
        }

        public ProvidedService()
        {
            IsActive = true;
        }

        public ProvidedService(ProvidedService service)
        {
            ProvidedServiceId = service.ProvidedServiceId;
            ServiceName = service.ServiceName;
            ServiceProvider = service.ServiceProvider;
            ServiceProviderId = service.ServiceProviderId;
            PricePerMeeting = service.PricePerMeeting;
            ServiceType = service.ServiceType;
            ServiceTypeId = service.ServiceTypeId;
            City = service.City;
            CityId = service.CityId;
            Days = service.Days;
            Hours = service.Hours;
            ImageSrc = service.ImageSrc;
            IsActive = service.IsActive;
        }
    }
}
