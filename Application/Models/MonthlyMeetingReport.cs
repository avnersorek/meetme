﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Models
{
    public class MonthlyMeetingReport
    {
        public DateTime ReportStart { get; set; }
        public DateTime ReportEnd { get; set; }
        public int TotalMeetingCount { get; set; }
        public int UniqueCustomerCount { get; set; }
        public int TotalRevenue { get; set; }
        public int ActualRevenue { get; set; }
        public Dictionary<Meeting.MeetingStatus, List<Meeting>> MeetingsByStatus { get; set; }
    }
}
