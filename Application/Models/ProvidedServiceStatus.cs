﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Models
{
    public class ProvidedServiceStatus : ProvidedService
    {
        public int NewMeetingsCount { get; set; }
        public int ApprovedMeetingsCount { get; set; }
        public int UnpaidMeetingsCount { get; set; }        

        public ProvidedServiceStatus(ProvidedService service) : base(service) { }

        public bool HasMeetings
        {
            get
            {
                return NewMeetingsCount > 0 || ApprovedMeetingsCount > 0 || UnpaidMeetingsCount > 0;
            }
        }
    }
}
