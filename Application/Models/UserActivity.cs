﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Models
{
    public class UserActivity
    {
        public int UserActivityId { get; set; }
        public DateTime ActivityTime { get; set; }
        public string Action { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
