﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MeetMe.Core.Models
{
    public class User
    {
        public enum RoleType { Admin, Customer, ServiceProvider };

        public int UserId { get; set; }
        public String HashedPassword { get; set; }
        public RoleType Role { get; set; }
        [Required(ErrorMessage="Email is required")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
    }
}
