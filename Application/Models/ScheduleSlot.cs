﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Models
{
    public class ScheduleSlot
    {
        public enum ScheduleStatus
        {
            Available, ServiceIsBusy, CustomerIsBusy, Closed
        }

        public DateTime Day { get; set; }
        public int Hour { get; set; }
        public ScheduleStatus Status { get; set; }
        public List<Meeting> Meetings { get; set; }
    }  
}
