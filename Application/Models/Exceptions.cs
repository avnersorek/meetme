﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Models
{
    public class MeetMeBaseException : Exception { }

    public class EntityNotFoundException : MeetMeBaseException { }
    public class WrongPasswordException : MeetMeBaseException { }
    public class UnauthorizedRoleException : MeetMeBaseException { }
    public class UnauthorizedResourceException : MeetMeBaseException { }
    public class EntityAlreadyExistsException : MeetMeBaseException { }
    public class AccountNotActiveException : MeetMeBaseException { }
    public class PasswordConfirmMismatchException : MeetMeBaseException { }
}
