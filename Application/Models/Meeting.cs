﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MeetMe.Core.Models
{
    public class Meeting  
    {
        public enum MeetingStatus { Scheduled, Approved, Cancelled, Paid }

        public int MeetingId { get; set; }
        public DateTime ScheduledTime { get; set; }
        public MeetingStatus Status { get; set; }

        [Required(ErrorMessage = "Provided Service is required")]
        public int ProvidedServiceId { get; set; }
        public ProvidedService ProvidedService { get; set; }

        [Required(ErrorMessage = "Customer is required")]
        public int CustomerId { get; set; }
        public User Customer { get; set; }

        public Meeting()
        {
            Status = Meeting.MeetingStatus.Scheduled;
        }
    }
}
