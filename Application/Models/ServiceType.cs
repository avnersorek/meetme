﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Models
{
    public class ServiceType
    {
        public int ServiceTypeId { get; set; }
        public string Name { get; set; }
    }
}
