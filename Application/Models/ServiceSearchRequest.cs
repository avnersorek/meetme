﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MeetMe.Core.Models
{
    public class ServiceSearchRequest
    {
        [Required(ErrorMessage = "Service Type is required")]
        public int ServiceTypeId { get; set; }        
        [Required(ErrorMessage = "City is required")]
        public int CityId { get; set; }
        public int? MaxPricePerMeeting { get; set; }
        public List<int> Days { get; set; }
        public List<int> Hours { get; set; }
    }
}
