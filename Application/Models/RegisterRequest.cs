﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MeetMe.Core.Models
{
    public class RegisterRequest
    {
        [EmailAddress(ErrorMessage = "Email is required")]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Password Confirmation is required")]
        [Compare("Password", ErrorMessage="Password Confirmation is required")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }
        public Boolean IsServiceProvider { get; set; }
    }
}
