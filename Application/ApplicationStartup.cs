﻿using Microsoft.Extensions.DependencyInjection;

using MeetMe.Core.Interfaces;
using MeetMe.Core.Services;
using MeetMe.Core.Utilities;

namespace MeetMe.Core
{
    public class ApplicationStartup
    {
        public static IServiceCollection InjectServices(IServiceCollection services)
        {
            services.AddSingleton<IPasswordHasher, RNGPasswordHasher>();

            // Any service that uses the DB needs to be scoped
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProvidedServicesService, ProvidedServicesService>();
            services.AddScoped<IMeetingService, MeetingService>();
            services.AddScoped<ISchedulingService, SchedulingService>();            
            services.AddScoped<IDictionaryService, DictionaryService>();
            services.AddScoped<IUserActivityService, UserActivityService>();

            return services;
        }
    }
}
