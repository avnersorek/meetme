﻿using System;
using System.Security.Cryptography;

using MeetMe.Core.Interfaces;

namespace MeetMe.Core.Utilities
{
    public sealed class RNGPasswordHasher : IPasswordHasher
    {
        private const int SaltSize = 16;
        private const int HashSize = 20;
        private const int Iterations = 1000;
        private const String HashPrefix = "$MYHASH$V1$";
        private const String HashDivider = "$";

        private byte[] _getHash(String password, byte[] salt)
        {            
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, Iterations);
            return pbkdf2.GetBytes(HashSize);
        }

        private byte[] _getNewSalt()
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[SaltSize]);
            return salt;
        }

        
        public string Hash(string password)
        {
            byte[] salt = _getNewSalt();          
            byte[] hash = _getHash(password, salt);

            byte[] saltAndHash = new byte[SaltSize + HashSize];
            Array.Copy(salt, 0, saltAndHash, 0, SaltSize);
            Array.Copy(hash, 0, saltAndHash, SaltSize, HashSize);
            
            var base64Hash = Convert.ToBase64String(saltAndHash);

            return String.Concat(HashPrefix, Iterations, HashDivider, base64Hash);
        }

        public bool Verify(string password, string hashedPassword)
        {
            if (!hashedPassword.Contains(HashPrefix))
            {
                throw new NotSupportedException("The hashtype is not supported");
            }
            
            var splittedHashString = hashedPassword.Replace(HashPrefix, String.Empty).Split(HashDivider);
            var iterations = int.Parse(splittedHashString[0]);
            var base64Hash = splittedHashString[1];

            var hashBytes = Convert.FromBase64String(base64Hash);

            var salt = new byte[SaltSize];
            Array.Copy(hashBytes, 0, salt, 0, SaltSize);

            byte[] hash = _getHash(password, salt);

            // compare
            for (var i = 0; i < HashSize; i++)
            {
                if (hashBytes[i + SaltSize] != hash[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
