﻿using System;
using System.Collections.Generic;
using System.Text;

using MeetMe.Core.Models;

namespace MeetMe.Core.Interfaces
{
    public interface IDictionaryService
    {
        void AddCity(City newCity);
        void AddServiceType(ServiceType newServiceType);
        List<City> GetCities();
        List<ServiceType> GetServiceTypes();
    }
}
