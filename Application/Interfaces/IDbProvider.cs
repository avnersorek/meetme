﻿using System;
using System.Collections.Generic;
using MeetMe.Core.Models;

namespace MeetMe.Core.Interfaces
{
    public interface IDbProvider<T>
    {
        List<T> FindAll(Func<T, bool> predicate);
        T Find(Func<T, bool> predicate);
        T Update(T updatedEntity);
        T Add(T newEntity);
    }
}