﻿using MeetMe.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Interfaces
{
    public interface IUserActivityService
    {
        void AddActivity(UserActivity newActivity);
        List<UserActivity> GetActivityForMonth(int year, int month);
    }
}
