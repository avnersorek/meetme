﻿using MeetMe.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MeetMe.Core.Interfaces
{
    public interface ILoginService
    {
        int GetLoggedInUserId();
        bool IsUserLoggedIn();
        Task LoginAsync(User user, bool persist);
        Task LogoutAsync();
    }
}
