﻿using System;
using System.Collections.Generic;
using System.Text;

using MeetMe.Core.Models;

namespace MeetMe.Core.Interfaces
{
    public interface IProvidedServicesService
    {
        ProvidedService AddService(ProvidedService newService);
        ProvidedService UpdateService(ProvidedService updatedService);
        ProvidedService GetServiceById(int serviceId);
        List<ProvidedServiceStatus> GetServiceStatusesForServiceProvider(int serviceProviderUserId);
        List<ProvidedService> SearchServices(ServiceSearchRequest searchDetails);
        void DeleteService(int serviceId);
        string GetServiceImagePlaceholder();
    }
}
