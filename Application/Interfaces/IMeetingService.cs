﻿using System;
using System.Collections.Generic;
using System.Text;

using MeetMe.Core.Models;

namespace MeetMe.Core.Interfaces
{
    public interface IMeetingService
    {
        List<Meeting> GetMeetingsForService(int providedServiceId, DateTime startTime, DateTime endTime);
        List<Meeting> GetMeetingsForService(int providedServiceId, Meeting.MeetingStatus[] statuses);
        List<Meeting> GetMeetingsForCustomer(int customerUserId);
        Meeting AddMeeting(int providedServiceId, int customerUserId, DateTime scheduledTime);
        Meeting GetMeetingById(int meetingId);
        void UpdateMeetingStatus(int meetingId, Meeting.MeetingStatus newStatus);
        MonthlyMeetingReport GetMonthlyMeetingReport(int providedServiceId, int year, int month);
    }
}
