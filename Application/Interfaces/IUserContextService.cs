﻿using MeetMe.Core.Models;
using System.Threading.Tasks;

namespace MeetMe.Core.Interfaces
{
    public interface IUserContextService
    {
        bool IsUserLoggedIn();
        User GetLoggedInUser();
        int GetLoggedInUserId();
        IRoutingStrategy GetRoutingStrategy();
        Task LoginAsync(User user, bool persist);
    }
}