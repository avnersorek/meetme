﻿using MeetMe.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMe.Core.Interfaces
{
    public interface IUserService
    {
        User CreateUser(RegisterRequest request);
        User VerifyPasswordAndGetUser(string email, string password);
        User GetUserById(int id);
        User UpdateUser(User changedUser);
        void UpdatePassword(int customerId, string newPassword);
        List<User> GetUsers();
    }
}
