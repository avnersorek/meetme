﻿using System.Collections.Generic;

namespace MeetMe.Core.Interfaces
{
    public interface IRoutingStrategy
    {
        string GetHomeRoute();
        List<(string, string)> GetSiteMenuItems();
    }
}