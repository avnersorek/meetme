﻿using System;
using System.Collections.Generic;
using MeetMe.Core.Models;

namespace MeetMe.Core.Interfaces
{
    public interface ISchedulingService
    {
        List<(int, string)> GetDays();
        List<DateTime> GetDaysInRange(DateTime startTime, DateTime endTime);
        List<(int, string)> GetHours();
        List<(string, ScheduleSlot[])> GetScheduleForService(int providedServiced, int customerId, DateTime startTime, DateTime endTime);
        List<(string, ScheduleSlot[])> GetScheduleForUser(int userId, DateTime startTime, DateTime endTime);
    }
}